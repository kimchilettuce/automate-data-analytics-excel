Option Explicit

' ----- Global variables ---------

' Array holding integer counts for each team
Dim apprenticeCount() As Integer
Dim EICount() As Integer
Dim NICount() As Integer
Dim MODCount() As Integer
Dim TFSCount() As Integer

' Each teamCount Array has particular counts stored in each index.
' These are listed below: 
'       0: Employee count Jan-Jun
'       1: Employee count Jul-Dec
'       2: Employee SW Jan-Jun
'       3: Employee SW Jul-Dec
'       4: Employee SAFE Jan-Jun
'       5: Employee SAFE Jul-Dec

'       6: Subcon count Jan-Jun
'       7: Subcon count Jul-Dec
'       8: Subcon SW Jan-Jun
'       9: Subcon SW Jul-Dec
'       10: Subcon SAFE Jan-Jun
'       11: Subcon SAFE Jul-Dec

'       12: Employee FPSSS
'       13: Subcon FPSSS

Dim state As String


Sub mainOverview()
'***************************************************************************
' Purpose: To read through the state data, count the number of workers who
' have completed their QW, FPSSS, SAFE etc.
'***************************************************************************
    On Error GoTo Cleanup

    ReDim apprenticeCount(0 To 13) As Integer
    ReDim EICount(0 To 13) As Integer
    ReDim NICount(0 To 13) As Integer
    ReDim MODCount(0 To 13) As Integer
    ReDim TFSCount(0 To 13) As Integer
    
    Dim overviewSheet As Worksheet
    Dim targetSheet As Worksheet
    Dim states(0 To 5) As String
    Dim state As Variant
    
    Application.Calculation = xlCalculationManual
    Application.ScreenUpdating = False
    Application.DisplayStatusBar = False
    Application.EnableEvents = False

    states(0) = "NSW"
    states(1) = "ACT"
    states(2) = "WA"
    states(3) = "SA"
    states(4) = "QLD"
    states(5) = "VIC"

    Set overviewSheet = ThisWorkbook.ActiveSheet
    
    ' This macro can be called by either state overview or nation overview
    If formatStr(ActiveSheet.name) = formatStr("Nation Overview") Then
        ' For nation, want to collate the count for each state
        For Each state In states
            Set targetSheet = ThisWorkbook.workSheets(state + " Data")
            Call extractValue(targetSheet)
        Next state
    
    ElseIf formatStr(ActiveSheet.name) = formatStr("State Overview") Then
    
        ' Only want to collate the count for this one state.
        state = ActiveSheet.ComboBox1.value
        Set targetSheet = ThisWorkbook.workSheets(state + " Data")
        
        Call extractValue(targetSheet)
    End If
    
    ' Updating the values to visually show on the Overview Sheet
    Call updateValue(overviewSheet, "C3", apprenticeCount)
    Call updateValue(overviewSheet, "C4", EICount)
    Call updateValue(overviewSheet, "C5", NICount)
    Call updateValue(overviewSheet, "C6", MODCount)
    Call updateValue(overviewSheet, "C7", TFSCount)


Cleanup:
    Erase apprenticeCount
    Erase EICount
    Erase NICount
    Erase MODCount
    Erase TFSCount

    With Application
        .Calculation = xlCalculationAutomatic
        .ScreenUpdating = True
        .DisplayStatusBar = True
        .EnableEvents = True
    End With
    
    If Err.Number <> 0 Then
        MsgBox "Import_Click Error: (" & Err.Number & ") " & Err.Description, vbCritical
    Else
        MsgBox ("Macro completed, values updated are indicated in light blue.")
    End If

End Sub


Private Sub addtoArray(teamName As String, idxAdd As Integer)
'***************************************************************************
' Purpose: Helper function, to collate counts for workers into arrays. Each 
' index in each teamArray is used to count particular activities. Refer to 
' table placed at top of this file.
' Inputs: 
'       teamName    -> (String)     Counts are split by team.
'       idxAdd      -> (Integer)    Specific entries have a particular idx
'                                   in the teamArray to add to.
'***************************************************************************
    ' spaces on the left and right end are << necessary >>
    ' this is to catch all cases for the regex
    teamName = " " + teamName + " "
    
    ' Determines which array count data to increment
    ' apprentices, ei, ni, etc. are all team names
    If LCase(teamName) Like "* apprentices *" Then
        apprenticeCount(idxAdd) = apprenticeCount(idxAdd) + 1

    ElseIf LCase(teamName) Like "* ei *" Then
        EICount(idxAdd) = EICount(idxAdd) + 1
    
    ElseIf LCase(teamName) Like "* ni *" Then
        NICount(idxAdd) = NICount(idxAdd) + 1

    ElseIf LCase(teamName) Like "* mods *" Then
        MODCount(idxAdd) = MODCount(idxAdd) + 1

    ElseIf LCase(teamName) Like "* tfs *" Then
        TFSCount(idxAdd) = TFSCount(idxAdd) + 1
    End If
End Sub


Private Sub extractValue(ws As Worksheet)
'***************************************************************************
' Purpose: Helper sub, used to collate the counts from the state data sheet.
' Inputs: 
'       ws    -> (Worksheet)     Worksheet containing the state data.
'***************************************************************************
    Dim position As String
    Dim teamName As String
    
    Dim idxArrayAdd As Integer
    Dim baseIdx As Integer

    ' these are the colours from state data that the macro will count
    ' currently they are the colours: green and dark blue
    Dim validColours(0 To 1) As Integer
    validColours(0) = settings.Range("B3").Interior.ColorIndex
    validColours(1) = settings.Range("B4").Interior.ColorIndex

    ws.Activate
    Range("A4").Select
    
    Do Until IsEmpty(ActiveCell)
    ' Check active cell for search value.

        ' Determines which position in the array
        position = Range("D" & ActiveCell.Row).value
        teamName = ActiveCell.value

        ' Dealing with employee count
        If position <> "Subcontractor" Then
            baseIdx = 0
        Else
            baseIdx = 6
        End If

        ' Current implementation is that subcon/employee count is identical 
        ' for (jan-jun) and (jul-dec).
        ' Hence, adding count to both count for jan-jun and jul-dec
        Call addtoArray(teamName, baseIdx)
        idxArrayAdd = baseIdx + 1
        Call addtoArray(teamName, idxArrayAdd)


        ' Dealing with SW Jan-Jun
        If colorIndexValid(Range(Trim(settings.Range("F42").value) & ActiveCell.Row), validColours) Then
            idxArrayAdd = baseIdx + 2
            Call addtoArray(teamName, idxArrayAdd)
        End If

        ' Dealing with SW Jul-Dec
        If colorIndexValid(Range(Trim(settings.Range("F45").value) & ActiveCell.Row), validColours) Then
            idxArrayAdd = baseIdx + 3
            Call addtoArray(teamName, idxArrayAdd)
        End If

        ' Dealing with SAFE Jan-Jun
        If colorIndexValid(Range(Trim(settings.Range("F43").value) & ActiveCell.Row), validColours) Then
            idxArrayAdd = baseIdx + 4
            Call addtoArray(teamName, idxArrayAdd)
        End If

        ' Dealing with SAFE Jul-Dec
        If colorIndexValid(Range(Trim(settings.Range("F46").value) & ActiveCell.Row), validColours) Then
            idxArrayAdd = baseIdx + 5
            Call addtoArray(teamName, idxArrayAdd)
        End If

        ' Dealing with FPSSS
        If colorIndexValid(Range(Trim(settings.Range("F48").value) & ActiveCell.Row), validColours) Then

            ' Recall the indexes for FPSSS in the teamcount arrays
            ' 12: Employee FPSSS
            ' 13: Subcon FPSSS

            ' For employee
            If baseIdx = 0 Then
                idxArrayAdd = 12
            ' For Subcontractor
            Else
                idxArrayAdd = 13
            End If

            Call addtoArray(teamName, idxArrayAdd)
        End If

        ' Goes to next row
        ActiveCell.Offset(1, 0).Select
    Loop
End Sub


Private Sub updateValue(ws As Worksheet, startCoord As String, ByRef inputArray() As Integer)
'***************************************************************************
' Purpose: Having populated the counts in the team arrays, we want to then 
' display this on the overview sheet.
' Inputs:
'       updateValue -> (Worksheet)      The overview worksheet to print on
'       startCoord  -> (String)         The range str of where Jan-Jun 
'                                       employees starts.
'       inputArray  -> (Integer Array)  The array containing the counts for 
'                                       the specific team.
'***************************************************************************
    ' This colour will mark the cells that got their values updated by this sub.
    Dim colour As Integer: colour = 34

    ws.Activate
    Range(startCoord).Select
    
    ' Employee Count
    ActiveCell.FormulaR1C1 = inputArray(0)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Epmloyee SW Jan-Jun
    ActiveCell.FormulaR1C1 = inputArray(2)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Epmloyee SAFE Jan-Jun
    ActiveCell.FormulaR1C1 = inputArray(4)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    'Subcon Count
    Range("H" & ActiveCell.Row).Select
    ActiveCell.FormulaR1C1 = inputArray(6)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Subcon SW Jan-Jun
    ActiveCell.FormulaR1C1 = inputArray(8)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Subcon SAFE Jan-Jun
    ActiveCell.FormulaR1C1 = inputArray(10)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' --------------------------------------------------

    ' Employee Count
    Range("C" & (ActiveCell.Row + 10)).Select
    ActiveCell.FormulaR1C1 = inputArray(1)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Epmloyee SW Jul-Dec
    ActiveCell.FormulaR1C1 = inputArray(3)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Epmloyee SAFE Jul-Dec
    ActiveCell.FormulaR1C1 = inputArray(5)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    'Subcon Count
    Range("H" & ActiveCell.Row).Select
    ActiveCell.FormulaR1C1 = inputArray(7)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Subcon SW Jul-Dec
    ActiveCell.FormulaR1C1 = inputArray(9)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Subcon SAFE Jul-Dec
    ActiveCell.FormulaR1C1 = inputArray(11)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' ---------------- Doing FPSSS ---------------------
    Range(startCoord).Select
    ActiveCell.Offset(20, 0).Select
    ActiveCell.FormulaR1C1 = inputArray(0)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Doing FPSSS employeee
    ActiveCell.FormulaR1C1 = inputArray(12)
    ActiveCell.Interior.ColorIndex = colour

    ' Doing subcontractor
    Range("H" & ActiveCell.Row).Select
    ActiveCell.FormulaR1C1 = inputArray(6)
    ActiveCell.Interior.ColorIndex = colour
    ActiveCell.Offset(0, 1).Select

    ' Doing FPSSS subcon
    ActiveCell.FormulaR1C1 = inputArray(13)
    ActiveCell.Interior.ColorIndex = colour
End Sub


Function colorIndexValid(cellTarget As Range, validColours() As Integer) As Boolean
'***************************************************************************
' Purpose: Checks if cellTarget has a valid colour to be included in the 
' overview count. By default, these colours are green (automatic entry) and 
' dark blue (manual entry).
'***************************************************************************
    Dim cellColour As Integer
    Dim colour As Variant
    
    cellColour = cellTarget.Interior.ColorIndex
    
    For Each colour In validColours
        If colour = cellColour Then
            colorIndexValid = True
            Exit Function
        End If
    Next colour
    
    colorIndexValid = False
    
End Function


Function formatStr(inputStr As String) As String
'***************************************************************************
' Purpose: Used to simplify the common use of turning strings to lowercase 
' and stripping trailing spaces. This creates more precise behaviour when
' comparing strings.
'***************************************************************************
    formatStr = Trim(LCase(inputStr))
End Function

