
' --------------- Global Variables -----------------------------------

Dim startRow As Integer

Sub initGlobalValues()
    startRow = 7
End Sub

' --------------- Clear Functions for Missing Data Sheet --------------

Sub clearMissingMsg()
'***************************************************************************
'Purpose: Used to clear the cells (contents and colour) of the missing data
' region in the "Missing Data" sheet. It also searches for entries that are
' marked in missing data as resolved/handled and sends it to the handled region
'***************************************************************************
    Call initGlobalValues
    
    Dim lastRow As Integer
    Dim strStartCol As String: strStartCol = "A"
    Dim strEndCol As String: strEndCol = "H"
    Dim rngStr As String
    
    Dim cell As Variant
    Dim sendRow As Range
    
    With missingData
        lastRow = .Cells(.Rows.Count, strStartCol).End(xlUp).Row
        rngStr = strStartCol + Trim(Str(startRow)) + ":" + strEndCol + Trim(Str(lastRow))
        
        ' Looks for cells in missingData that are marked to be sent to handled
        ' Note: only one cell per row should be marked, else that entry it sent
        ' multiple times to handled.
        For Each cell In .Range(rngStr)
            ' The particular marker colour is specified in the Missing Data sheet
            If cell.Interior.ColorIndex = .Range("B1").Interior.ColorIndex Then
                Set sendRow = .Range(strStartCol + Trim(Str(cell.Row)) + ":" + strEndCol + Trim(Str(cell.Row)))
                Call sendToHandled(sendRow)
            End If
            
        Next cell
    End With
    
    Call clearRange(strStartCol, strEndCol)
    
End Sub

Sub clearDataMsg()
'***************************************************************************
'Purpose: Same purpose as clearMissingMsg, but much simpler since it doesn't
' check for entries to be sent to handled.
'***************************************************************************
    Call initGlobalValues
    
    Dim strStartCol As String: strStartCol = "I"
    Dim strEndCol As String: strEndCol = "P"
    
    Call clearRange(strStartCol, strEndCol)
    
End Sub


Sub sendToHandled(data As Range)
'***************************************************************************
'Purpose: Converts the range entry into a single comma separated string.
' This string is then appended to the end of the handled entries region.
' This is so the same entry will not appear in missing data again.
'Inputs: data -> (Range) Contains the row range of the entry from missing data region
'***************************************************************************
    Dim lastRow As Integer
    Dim strStartCol As String: strStartCol = "Q"
    Dim strEndCol As String: strEndCol = "X"
    
    Dim myArr As Variant
    Dim regEmpty As New RegExp
    Dim res As String
    
    regEmpty.Pattern = "empty"
    regEmpty.Global = True
    
    ' Stack Overflow suggestion to convert range into an array
    myArr = Application.WorksheetFunction.Transpose(Application.WorksheetFunction.Transpose(data))
    
    With missingData
        lastRow = .Cells(.Rows.Count, strStartCol).End(xlUp).Offset(1).Row
        
        ' Joining array to be a single string
        res = Join(myArr, ",")
        
        ' Replacting any strings that contain "empty" to instead be empty strings ""
        ' This is to create consistent format
        res = regEmpty.Replace(res, "")
        
        ' Note: also found that that there was an extra comma, so want to use left
        ' function to ignore the last extra comma
        res = Left(res, Len(res) - 1)
        
        .Range(strStartCol + Trim(Str(lastRow))).value = res
    End With
    
End Sub


Sub clearRange(strStartCol As String, strEndCol As String)
'***************************************************************************
'Purpose: Using global variable for starting row, clear the contents and
' colour for a range from strStartCol to strEndCol
'Inputs:
'***************************************************************************
    Dim startRowStr As String: startRowStr = Trim(Str(startRow))
    Dim lastRow As Integer
    
    ' currRegion method can't be used since the regions are touching each other
    With missingData
        lastRow = .Cells(.Rows.Count, strStartCol).End(xlUp).Offset(1).Row
        .Range(strStartCol + startRowStr + ":" + strEndCol + Trim(Str(lastRow))).ClearContents
        .Range(strStartCol + startRowStr + ":" + strEndCol + Trim(Str(lastRow))).Interior.ColorIndex = 0
    End With

End Sub


' ------------- Clearing for State Data ------------------------

Sub clearStateGreen()
'***************************************************************************
'Purpose: Used to clear the marked cells (colour and contents) in a state's
' data sheet.
'Inputs:
'***************************************************************************
    
    Dim targetRng As Range
    Dim cell As Variant
    Dim green As Integer
    Dim lastRow As Integer
    
    Dim strStartCol As String: strStartCol = "E"
    Dim strEndCol As String: strEndCol = "K"
    Dim strStartRow As String: strStartRow = "4"
    
    With ActiveSheet
        lastRow = .Cells(.Rows.Count, "A").End(xlUp).Row
        
        ' (lastRow - 2) is used below since 2 rows at the bottom reserved for monthly/quarterly totals
        Set targetRng = .Range(strStartCol + strStartRow + ":" + strEndCol + Trim(Str(lastRow - 2)))
    End With
    
    ' the colour marked can be changed by the user
    green = settings.Range("B3").Interior.ColorIndex
    
    ' State sheets also contain numbers indicating the QW count
    targetRng.ClearContents
    
    ' Remove colour << only for >> the green. Other colours are reserved for manual marking
    For Each cell In targetRng
        If cell.Interior.ColorIndex = green Then
            cell.Interior.ColorIndex = 0
        End If
    Next cell
    
End Sub


' ------------------------------------------

Sub clearOverview()
'***************************************************************************
'Purpose: Macro invoked when "Clear Counts" button is pressed on the overview
' sheets.
'***************************************************************************

    Dim targetRng As Range
    
    Dim rangesArray As Variant
    Dim strRange As Variant
    
    rangesArray = Array("C3:E7", "H3:J7", "C13:E17", "H13:J17", "C23:D27", "H23:I27")
    
    For Each strRange In rangesArray
        Set targetRng = ActiveSheet.Range(strRange)
        targetRng.Clear
        targetRng.value = 0
    Next strRange
    
End Sub





