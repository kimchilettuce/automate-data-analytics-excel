Option Explicit

' --------- Global Variables -------

Dim currYear As Integer
Dim state As String


Sub mainStateData()
'***************************************************************************
' Purpose: To read through the SRS reports and mark the activities completed
' by each worker on the state data sheets.
'***************************************************************************
    'Loop through all files in a folder
    Dim fileName As Variant
    Dim basePath As String
    Dim externalBook As Workbook
    Dim data2DArray() As Variant
    Dim fileCount As Integer: fileCount = 0
    Dim arrWorkSheets As Variant
    Dim sheetName As Variant
    
    ' Sub setup
    On Error GoTo Cleanup
    Application.Calculation = xlCalculationManual
    Application.ScreenUpdating = False
    Application.DisplayStatusBar = False
    Application.EnableEvents = False

    ' An Array of all the states sheet names
    arrWorkSheets = Array("NSW Data", "QLD Data", "SA Data", _
        "WA Data", "VIC Data", "ACT Data")

    Debug.Print " -------- Starting ----------- "
    Debug.Print ""
    
    ' --------- Setting up year and state -----------
    
    currYear = Year(Date)
    
    ' Update Value macro can be called from either the state sheet or the nation sheet
    ' Calling it from the nation sheet, runs update value on all states
    ' If ThisWorkbook.ActiveSheet.name = "Nation Overview" Then
    If ThisWorkbook.ActiveSheet Is shtNationOverview Then
        state = "Nation"
    Else
        Call getState(ThisWorkbook.ActiveSheet)
    End If
    
    ' Starting Message
    MsgBox "This macro updates the green boxes coloured." & _
        vbNewLine & vbNewLine & "Select a date from which new entries after the date from SRS reports will be added to master." _
        & vbNewLine & vbNewLine & "Data is extracted for " & state & " in the year " & Str(currYear)
        

    ' ---------------------- Finding User Date ------------------------
    
    ' Open user input for startingDate
    Dim dateVariable As Date
    dateVariable = CalendarForm.GetDate
    
    ' Confirm user input for date
    Dim ans As Integer
    ans = MsgBox("Are you sure with the selected date " + Str(dateVariable) & _
        vbNewLine & "Date is in format: month/day/year", vbYesNoCancel)
    
    ' Keep looping until user confirms input or user cancels macro
    Do While (ans <> 6)
        If ans = 7 Then
            dateVariable = CalendarForm.GetDate
            ans = MsgBox("Are you sure with the selected date " + Str(dateVariable) & _
                vbNewLine & "Date is in format: month/day/year", vbYesNoCancel)
        ElseIf ans = 2 Then
            GoTo Cleanup
        Else
            GoTo Cleanup
        End If
    Loop
    
    ' ------------------------ Looping through files -------------------------
    
    ' Finding the folder
    basePath = workSheets("Settings").Cells(2, "B").value + "\"
    currYear = 2021
    ' state = Worksheets("Settings").Cells(3, "B").value
    fileName = Dir(basePath + "*")


    While fileName <> ""
        
        Set externalBook = Workbooks.Open(fileName:=basePath + fileName, ReadOnly:=True)
        
        ' Skip any files that are not recognised
        If LCase(externalBook.name) Like "ngfe *" = False And _
            LCase(externalBook.name) Like "safety walk *" = False And _
            LCase(externalBook.name) Like "*fatality prevention strategy *" = False And _
            LCase(externalBook.name) Like "quality walks *" = False Then
            GoTo main__next_file
        End If

        ' Print to debug the files that are read
        Debug.Print basePath + fileName

        ' If Macro called from Nation Overview, want data to be
        ' updated for all States.
        If ThisWorkbook.ActiveSheet Is shtNationOverview Then
            
            Dim currSheet As Worksheet
            
            ' Loop through each data sheet for each state
            For Each sheetName In arrWorkSheets
                ' First, get the state data sheet itself. and determine the state
                Set currSheet = ThisWorkbook.Sheets(sheetName)
                Call getState(currSheet)

                ' extract data from SRS reports and then update it unto the state sheet.
                data2DArray = getData(externalBook, dateVariable)
                Call updateMaster(currSheet, data2DArray)
            Next sheetName

            MsgBox externalBook.name + " completed"

        ' Only want to update for the single state
        Else
            data2DArray = getData(externalBook, dateVariable)
            Call updateMaster(ThisWorkbook.ActiveSheet, data2DArray)
        End If

        ' Count num files opened for the macro completion message to user
        fileCount = fileCount + 1

main__next_file:
        ' Call updateData, set save=False
        externalBook.Close (False)

        'Set the fileName to the next file, loops to next file
        fileName = Dir
    Wend
    
Cleanup:
    With Application
        .Calculation = xlCalculationAutomatic
        .ScreenUpdating = True
        .DisplayStatusBar = True
        .EnableEvents = True
    End With
    
    ' Err number 9 is when the workbook is already open, and you try to open it from this macro
    If Err.Number = 9 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Error: (" & Err.Number & ") " & Err.Description, vbCritical
    Else
        MsgBox ("Macro completed with " + Str(fileCount) + " files opened.")
    End If
    
End Sub


Private Function getData(externalBook As Workbook, dateVar As Date) As Variant
'***************************************************************************
' Purpose:  Used to extract Data from an excel workbook opened.
' Note:     It is required that the global variable state is set before 
'           using this function. This is so we know what entries to extract
'           from the SRS reports.
'***************************************************************************
    Dim dumpRange As Range
    Dim externalSheet As Worksheet
    Dim settingsSheet As Worksheet

    Dim rowCount As Long
    Dim rowCountStr As String
    Dim lastRow As Long
    
    Dim location As String
    Dim formatName As String    ' Name in format: "Lastname, firstname"
    
    Dim userArray(0 To 6) As Variant
    Dim userList() As Variant
    Dim userListCount As Integer
    
    Dim midYearDate As Date: midYearDate = DateValue("July 1, " + Str(currYear))
    Dim userDate As Date
    Dim userDateString As String
    Dim userActivity As String
    
    Dim dateCheckCol As Integer
    Dim locationCol As Integer
    Dim nameCol As Integer
    Dim positionCol As Integer
    Dim backupNameCol As Integer

    ' Column variables specific to QW Reports
    Dim ytdCol As Integer: ytdCol = 19
    Dim yTargetCol As Integer: yTargetCol = 1
    
    ' Create regex for location, might need to make pattern more specific
    Dim rgxLocation As New RegExp
    rgxLocation.Pattern = state
    
    Debug.Print " --------- Collecting Data ----- "
    
    ' This sub extracts data and puts each user's data into an array
    ' 0:    Name
    ' 1:    Position
    ' 2:    Report Activity Type and Date
    ' 3:    Where to Find the particular entry
    ' 4:    State
    ' 5:    Count Jan-Jun (Specifically used for QW)
    ' 6:    Count Jul-Dec (Specifically used for QW)

    ' ------- Setup ----------
    On Error GoTo PROC_ERR_DATA
    
    ' externalBook is the report we are opening. Hence the specific sheet we
    ' want to extract data from is the first worksheet in that report.
    Set externalSheet = externalBook.workSheets(1)
    Set settingsSheet = ThisWorkbook.workSheets("Settings")
    
    ' Determining the type of activity completed
    ' And setting up which column to check for date
    ' The two reports have date in different columns
    With settingsSheet
        If LCase(externalBook.name) Like "ngfe *" Then
            userActivity = "SAFE "
            
            dateCheckCol = .Cells(9, "E").value
            locationCol = .Cells(10, "E").value
            nameCol = .Cells(11, "E").value
            backupNameCol = .Cells(12, "E").value
            positionCol = .Cells(13, "E").value

        ElseIf LCase(externalBook.name) Like "safety walk *" Then
            userActivity = "SW "
            
            dateCheckCol = .Cells(2, "E").value
            locationCol = .Cells(3, "E").value
            nameCol = .Cells(4, "E").value
            backupNameCol = .Cells(5, "E").value
            positionCol = .Cells(6, "E").value

        ElseIf LCase(externalBook.name) Like "*fatality prevention strategy *" Then
            userActivity = "FPSSS "
            
            dateCheckCol = .Cells(16, "E").value
            locationCol = .Cells(17, "E").value
            nameCol = .Cells(18, "E").value
            backupNameCol = .Cells(19, "E").value
            positionCol = .Cells(20, "E").value
        
        ElseIf LCase(externalBook.name) Like "quality walks *" Then
            
            ' Has a different sheet location from the others
            Set externalSheet = externalBook.workSheets("Technician Leader Board")
            
            userActivity = "QW "
            
            dateCheckCol = .Cells(23, "E").value
            locationCol = .Cells(24, "E").value
            nameCol = .Cells(25, "E").value
            backupNameCol = .Cells(26, "E").value
            positionCol = .Cells(27, "E").value

        Else
            Err.Raise vbObjectError + 513, externalBook.name, "Not recognised report type"
        End If
    End With
    
    
    With externalSheet
    
    ' Make sure all filters on the columns are turned off
    .ShowAllData
    
    ' This particular report needs a offset for the lastRow
    lastRow = .Cells(.Rows.Count, "A").End(xlUp).Row
    If userActivity = "QW " Then
        lastRow = lastRow - 2
    End If
    
    ' Setting up rowCount. The count starts from the bottom of the report,
    ' going up.
    Dim i As Integer: i = 0
    rowCount = lastRow - i
    
    ' -------------------------
    
    ' Cycle through each entry in the report.
    Do While True
        
        rowCount = lastRow - i
        
        ' --------- Loop break conditions ------------------------

        If rowCount < 2 Then
            GoTo getData__loop_exit
        End If

        ' Check the date is within the timebracket
        ' if the cells date is before the target, then don't consider it
        If dateCheckCol <> -1 Then
            If .Cells(rowCount, dateCheckCol).value < dateVar Then
                GoTo getData__loop_continue
            End If

        ' If dateCheckcol is set to -1, e.g. QW reports, since it doesn't have a date
        Else
            ' Skip the rows where it is calculating totals for the service leader
            ' Or: if the number of Quality Walks they have done in the year is 0
            If .Cells(rowCount, nameCol).value = "TOTAL" Or .Cells(rowCount, ytdCol) = 0 Then
                GoTo getData__loop_continue
            End If
        End If

        ' If location not in state, go to next
        location = .Cells(rowCount, locationCol).value
        ' Handling the case where it is a different state, but not headquarters
        If rgxLocation.test(location) = False And location Like "Headquarter*" = False Then
            GoTo getData__loop_continue
        End If
        
        ' --------- Dealing with state -----------

        ' Specifically handling headquarters case
        If location Like "Headquarter*" = True Then
            userArray(4) = "HQ"
        Else
            userArray(4) = state
        End If
        
        ' ------------- Dealing with the name ---------------------
        
        ' Want separate logic for FPSSS report, since they have
        ' separate columns for first and last name
        If LCase(externalBook.name) Like "*fatality prevention strategy *" Then
            Dim firstName As String
            Dim lastName As String

            firstName = Trim(.Cells(rowCount, nameCol).value)
            lastName = Trim(.Cells(rowCount, backupNameCol).value)
            
            ' first put the name is default format "firstName lastName"
            formatName = firstName + " " + lastName

        ' Logic for NGFE and SW and QW
        Else
            formatName = .Cells(rowCount, nameCol).value

            ' If name is empty, then try checking if name is stored in backup column
            If Trim(formatName) = "" Then
                formatName = .Cells(rowCount, backupNameCol).value
            End If
            
            ' since some entries in QW have a (SUB) in their name
            ' gets rid of this
            removeSUBsuffix formatName
        End If

        ' uniqueName takes the input name byref
        ' For both NGFE/SW and FPSSS, you still want to check uniqueNames
        ' and you also want to else format the names as "lastName + firstName"
        If uniqueName(settingsSheet, formatName) = False Then
            formatName = doFormatName(formatName)
        End If

        ' storing this finally formatted name in userArray(0)
        userArray(0) = formatName
        
        ' ------------ Handling the date and position/role ----------------
        
        ' get date value and categorise wheter it is before midyear or after midyear
        If dateCheckCol <> -1 Then
            userDate = .Cells(rowCount, dateCheckCol).value
            If userDate >= midYearDate Then
                userDateString = "Jul - Dec"
            Else
                userDateString = "Jan - Jun"
            End If
            
            userArray(2) = userActivity + userDateString
            userArray(1) = .Cells(rowCount, positionCol).value
            
        ' Logic for QW reports that don't have dates per row
        ' Note the summing of data below is prone to error if columns in Quality
        ' Walk report is changed
        Else
            userArray(2) = userActivity + "with target: " + _
                Trim(Str(.Cells(rowCount, yTargetCol).value))
            
            ' The casted string needs to be trimmed
            rowCountStr = Trim(Str(rowCount))
            
            ' sum the number of quality walks done in jan-jun
            userArray(5) = WorksheetFunction.Sum(.Range("G" + rowCountStr + ":L" + _
                rowCountStr))

            ' sum the number of quality walks done in jul-dec
            userArray(6) = WorksheetFunction.Sum(.Range("M" + rowCountStr + ":R" + _
                rowCountStr))
            
            ' for QW set there to be no position for users
            userArray(1) = ""
        End If

        ' --------------------------------------------------------------

        ' Adding information on where to find the particular entry in case of error
        userArray(3) = externalBook.name + " at row: " + Str(rowCount)
      
        ' Dynamically grow the userList. The userList contains a list of userArrays(0), 
        ' where each userArray contains the summarised information of each worker
        ReDim Preserve userList(0 To userListCount)
        userList(userListCount) = userArray
        userListCount = userListCount + 1
        
        ' Debug.Print "Got data " + userArray(0)
        ' Debug.Print "Got data " + userArray(1)
        ' Debug.Print "Got data " + userArray(2)
        ' Debug.Print "Got data " + userArray(3)
        ' Debug.Print "Got data " + userArray(4)
        ' Debug.Print "Got data " + Str(userArray(5))
        ' Debug.Print "Got data " + Str(userArray(6))
        ' Debug.Print ""
        
getData__loop_continue:
        i = i + 1
    Loop

getData__loop_exit:
    End With
    
    ' Return the userList as an 2D array
    getData = userList

Exit Function

PROC_ERR_DATA:

    Select Case Err.Number
    
    ' Ignores error caused by no entries found
    Case 1004
        Resume Next
    Case Else
        MsgBox "getData Error: (" & Err.Number & ") " & Err.Description, vbCritical
    End Select

End Function


Private Sub updateMaster(stateDataSheet As Worksheet, userList() As Variant)
'***************************************************************************
' Purpose:  Takes in an array of userArrays and updates the information unto
' the state data sheet.
'***************************************************************************
    On Error GoTo PROC_ERR
    
    ' local variables
    Dim userArray As Variant
    Dim rowCount As Integer
    Dim rowLast As Integer
    Dim colCount As Integer
    Dim usersFound() As Variant
    Dim foundCount As Integer
    Dim colourTick As Integer
    Dim colourManual As Integer
    
    Debug.Print "----- Updating data ------ "
    
    ' ----------- Check that the data extracted is not empty --------
    
    If isInitialised(userList) = False Then
        MsgBox "No users were found"
        Exit Sub
    End If
    
    ' -----------------------
    
    colourTick = ThisWorkbook.workSheets("Settings").Range("B3").Interior.ColorIndex
    colourManual = ThisWorkbook.workSheets("Settings").Range("B4").Interior.ColorIndex
    
    With stateDataSheet
    
    ' Make sure all filters are unravelled
    ' the first worker starts on row 4 on the state data sheet.
    .ShowAllData
    rowLast = .Cells(.Rows.Count, "A").End(xlUp).Row
    rowCount = 4

    Do While rowCount <= rowLast
        
        For Each userArray In userList
        
            ' if the user has an error, userArray(0) will be "", so skip this one
            ' if the user's name does not match the name in .cells(...) then skip it to check the next entry
            If userArray(0) = "" Then
                GoTo updateMaster__nextfor
            ' Trim(LCase(.Cells(rowCount, 3)) is the name on the report
            ' Trim(LCase(userArray(0)))       is the name we extracted
            ElseIf Trim(LCase(userArray(0))) <> Trim(LCase(.Cells(rowCount, 3))) Then
                GoTo updateMaster__nextfor
            End If
            
            ' Debug.Print userArray(0) + " user found at " + Str(rowCount)
            
            ' Figure out which column to colour green
            If userArray(2) = "SAFE Jan - Jun" Then
                colCount = settings.Range("E43").value
            ElseIf userArray(2) = "SAFE Jul - Dec" Then
                colCount = settings.Range("E46").value
            ElseIf userArray(2) = "SW Jan - Jun" Then
                colCount = settings.Range("E42").value
            ElseIf userArray(2) = "SW Jul - Dec" Then
                colCount = settings.Range("E45").value
            ElseIf userArray(2) = "FPSSS Jul - Dec" Or userArray(2) = "FPSSS Jan - Jun" Then
                colCount = settings.Range("E48").value
            ' Quality Works Report has its own separate logic
            ElseIf userArray(2) Like "QW *" Then
                ' userArray(2) will be in the form "QW with target: _"
                ' We want to extract the target

                Dim splitStr As Variant
                Dim target As Variant
                Dim janJunCol As Integer: janJunCol = settings.Range("E44").value
                Dim julDecCol As Integer: julDecCol = settings.Range("E47").value
                
                ' Extract target from the string in userArray(2)
                splitStr = Split(userArray(2), " ")
                target = CDec(splitStr(UBound(splitStr)))
                
                ' Print out the values by default,
                ' the cells will only be coloured green if achieve target
                .Cells(rowCount, janJunCol).value = Str(userArray(5)) + "/" + Str(target)
                .Cells(rowCount, julDecCol).value = Str(userArray(6)) + "/" + Str(target)
                
                ' Colour in the cells, unlike the others, QW has two columns to colour
                If userArray(5) >= (target / 2) Then
                    .Cells(rowCount, janJunCol).Interior.ColorIndex = colourTick
                End If
                
                If userArray(5) + userArray(6) >= target Then
                    .Cells(rowCount, julDecCol).Interior.ColorIndex = colourTick
                End If
                
                GoTo updateMaster__colourDone
            Else
                Err.Raise Number:=1, Description:="not a valid value"
            End If
            
            ' colourTick is currently light green colorindex 43
            .Cells(rowCount, colCount).Interior.ColorIndex = colourTick
        
updateMaster__colourDone:
            ' Add the user to the foundList and also,
            ' dynamically grow the usersFound
            ReDim Preserve usersFound(0 To foundCount)
            usersFound(foundCount) = userArray(0)
            foundCount = foundCount + 1
            
            ' 0: Lastname, First
            ' 1: Position
            ' 2: Type (SAFE, Safety Walk Jan- Jun, ... etc)
            ' 3: rowCount in externalBook for missingdata
            
updateMaster__nextfor:
        Next

        rowCount = rowCount + 1
    Loop
    
    ' ------------ Colouring in process finished ------------------------
    
    Dim missingSheet As Worksheet
    Set missingSheet = ThisWorkbook.workSheets("Missing Data")
    
    ' Finding which user was not found in the state data sheet.
    ' If not found, send it to missingData, if it was found, send it to added.
    For Each userArray In userList
        Dim name As String: name = userArray(0)
        
        ' If user was found:
        If name <> "" And isInArray(name, usersFound) Then
            Call printMacroOutput(missingSheet, userArray, "I")
        ' If user was not found:
        Else
            Call printMacroOutput(missingSheet, userArray, "A")
        End If
    Next
        
    End With

Exit Sub

PROC_ERR:

    Select Case Err.Number
    
    ' Ignores error caused by no entries
    Case 1004
        Resume Next
    Case Else
        MsgBox "updateMaster Error: (" & Err.Number & ") " & Err.Description, vbCritical
    End Select
    
End Sub


' ------------ Sub Modules to Break Down Complexity ------------

Private Sub getState(ws As Worksheet)
'***************************************************************************
' Purpose:  Set the global variable state, depending on which state data 
' worksheet was opened.
'***************************************************************************
    Dim sheetTitle As String

    sheetTitle = ws.Cells(1, 1).value
    
    If sheetTitle Like "NSW *" Then
        state = "NSW"
    ElseIf sheetTitle Like "QLD *" Then
        state = "QLD"
    ElseIf sheetTitle Like "SA *" Then
        state = "SA"
    ElseIf sheetTitle Like "ACT *" Then
        state = "ACT"
    ElseIf sheetTitle Like "WA *" Then
        state = "WA"
    ElseIf sheetTitle Like "VIC *" Then
        state = "VIC"
    End If
End Sub

' This sub is used for when there is missing data that needs to be addressed.
Private Sub printMacroOutput(wsDest As Worksheet, userArray As Variant, startCol As String)
'***************************************************************************
' Purpose:  This is the sub that sends entries into missing data sheet, and
' sorts whether entry goes to <missing data> or <added>. If the entry is to be sent 
' to missing, it first checks if that entry already exists in <handled>.
' If already in handled, then don't send it to <missing> again.
'***************************************************************************
    Dim lastRow As Long
    
    Dim property As Variant
    Dim colCount As Integer: colCount = Asc(startCol) - 64 ' - 64 to convert string to int

    ' If sending to missingData, check first to see if it is handled
    ' Attempt to send current entry to missing data
    If startCol = "A" Then

        Dim userArrayStr As String
        Dim targetRng As Range
        Dim cell As Variant

        lastRow = wsDest.Cells(wsDest.Rows.Count, "Q").End(xlUp).Row
        userArrayStr = Join(userArray, ",")
        Set targetRng = wsDest.Range("Q7:Q" + Trim(Str(lastRow)))

        ' Going through each entry in the handled region, to see if it matches current entry
        For Each cell In targetRng
            If Trim(userArrayStr) = Trim(cell.value) Then
                Exit Sub
            End If
        Next cell
    End If

    ' Find first blank row in the destination range based on data in column A
    ' Offset property moves down 1 row
    lastRow = wsDest.Cells(wsDest.Rows.Count, startCol).End(xlUp).Offset(1).Row

    For Each property In userArray

        If property = "" Then
            property = "empty"
        End If

        wsDest.Cells(lastRow, colCount).value = property
        colCount = colCount + 1
    Next

End Sub


' ---------------- Functions/Subs to Format Name ------------------

Private Sub removeSUBsuffix(ByRef name As String)
'***************************************************************************
' Purpose:  Some names in QW reports have a "(SUB)" in their name. THis simple
' sub removes it from the name and returns the name by reference.
'***************************************************************************
    Dim rgxSub As New RegExp

    rgxSub.Pattern = "[(]SUB[)]"

    name = rgxSub.Replace(name, "")
    name = Trim(name)

End Sub


Private Function uniqueName(externalSheet As Worksheet, ByRef name As String) As Boolean
'***************************************************************************
' Purpose:  This function is used to search the name is a list of unique name
' patterns to apply manual formating. 
' Example: On the report the name is "Ben Radley", but on the state data sheet,
' the name is "Benjamin Radley". These sepcial cases are handled by this function.
' Note: The name is returned byreference. The return value is a Boolean to indicate
' a uniqueName pattern was found.
'***************************************************************************
    Dim lastRow As Integer
    Dim rng As Range, cell As Range
    
    lastRow = externalSheet.Cells(externalSheet.Rows.Count, "H").End(xlUp).Row
    Set rng = ThisWorkbook.workSheets("Settings").Range("H3:H" + Trim(Str(lastRow)))
    
    For Each cell In rng
        If Trim(LCase(cell.value)) = Trim(LCase(name)) Then
            name = cell.Offset(0, 1).value
            uniqueName = True
            Exit Function
        End If
    Next cell
    
    uniqueName = False
    
End Function


Private Function doFormatName(inputName As String) As String
'***************************************************************************
' Purpose:  To format name from SRS reports to match it according to state data
' Inputs:
'       inputName       ->  (String) In the form of "firstName [middleName] 
'                           lastName"
' Returns:
'       formattedName   ->  (String) This function generates a new string in
'                           the form of "lastName, firstName [middleName]"
'***************************************************************************
    Dim regEx As New RegExp
    Dim strArray() As String
    Dim frontStr As String
    
    strArray = Split(Trim(inputName))

    ' case: where contains middle name(s)
    If UBound(strArray) > 1 Then
        regEx.Pattern = strArray(0)
        frontStr = Trim(regEx.Replace(inputName, ""))
        doFormatName = Trim(frontStr + ", " + strArray(0))

    ' case: where it does not contain middle name
    ElseIf UBound(strArray) = 1 Then
        doFormatName = Trim(strArray(1) + ", " + strArray(0))

    ' case: where the string only contains one word
    ElseIf UBound(strArray) = 0 Then
        doFormatName = strArray(0)
    
    ' case: inputName is empty
    Else
        doFormatName = ""
    
    End If
    
End Function


' -------------------- Helper Functions ----------------------


Function isInArray(stringToBeFound As String, arr As Variant) As Boolean
'***************************************************************************
' Purpose:  Checks if string is found in an array of strings
' Inputs:
'       arr       ->  (Variant) An array of strings.
'***************************************************************************
    isInArray = (UBound(Filter(arr, stringToBeFound)) > -1)
End Function


Function isInitialised(ByRef a() As Variant) As Boolean
'***************************************************************************
' Purpose:  Checks if an array is empty. Returns a boolean to indicate whether
' or not the array has been initialised with contents, or remains empty.
'***************************************************************************
    On Error Resume Next
    isInitialised = IsNumeric(UBound(a))
    On Error GoTo 0
End Function


'---------------- Subs for testing ------------------

Private Sub testUniqueName()
    Dim externalSheet As Worksheet
    Dim isFound As Boolean
    Dim name As String
    
    name = "Christopher Nicolas"
    
    Set externalSheet = ThisWorkbook.workSheets("Settings")
    
    isFound = uniqueName(externalSheet, name)

End Sub


Private Sub testDoFormatName()
    Dim name As Variant
    Dim inputName As String
    Dim strings As Variant
    
    strings = Array("kinzey rahardja", "kinzey yo rahardja", "kinzey ", "")
    
    For Each name In strings
        inputName = name
        Debug.Print doFormatName(inputName)
    Next name
    
    Debug.Print "finished"
    
    
End Sub

Private Sub testFormat()

    Dim arr As Variant
    Dim item As Variant
    Dim regEx As New RegExp
    
    arr = Array("NSW Apprentices", "WA EI - Repairs", "WA EI - SL1", "yeet EIYO", "hello EI")
    regEx.Pattern = "WA EI"
    
    For Each item In arr
        
        If LCase(item + " ") Like "* ei *" Then
            Debug.Print "found " + item
        Else
            Debug.Print "not found " + item
        End If
        
    Next item
    
    Debug.Print
    
End Sub

Private Sub testForLoopWorksheets()

    Dim arrWorkSheets As Variant
    Dim sheetName As Variant
    arrWorkSheets = Array("NSW Data", "QLD Data", "SA Data", _
        "WA Data", "VIC Data", "ACT Data")
    
    For Each sheetName In arrWorkSheets
        Debug.Print ThisWorkbook.Sheets(sheetName).Range("A1").value
    Next sheetName


End Sub

Private Sub testGetState()
    Call getState(ThisWorkbook.Sheets("QLD Data"))
    
    Debug.Print state
End Sub

Private Sub testInteger()
    Dim num As Variant: num = CDec("4")
    Dim inum As Integer: inum = 2
    
    num = num / 2
    
    If inum >= num Then
        Debug.Print num
    Else
        Debug.Print "Not Equal"
    End If
    
End Sub

Private Sub testRemoveSUB()
    ' removeSUBsuffix(ByRef name As String)
    
    Dim arr As Variant
    Dim item As Variant
    Dim test As String
    
    arr = Array("Daniel Broadhurst (SUB)", "Peter(SUB) yo")

    
    For Each item In arr
        
        test = item
        Debug.Print "org: " + test
        
        removeSUBsuffix test
        
        
        Debug.Print "final: " + test
        
    Next item
    
    Debug.Print
    
End Sub












