Sub SpecialLoop()
    Dim row As Variant, rng As Range
    Dim V As Variant, i As Long
    
'    Set rng = Range("A2:Q14")
    ' <>*SUB *
    ActiveSheet.Range("A1").AutoFilter field:=5, Criteria1:="Jack Chadwick", VisibleDropDown:=True
    
    Debug.Print "done"
'    ' Allows us to manually set filters
'    For Each row In rng.SpecialCells(xlCellTypeVisible).Rows
'        ReDim V(1 To row.Cells.Count)
'        For i = 1 To row.Cells.Count
'            V(i) = row.Cells(1, i).Value
'        Next i
'        Debug.Print Join(V, " ")
'    Next row

End Sub


' https://stackoverflow.com/questions/53426986/vba-autofilter-not-equal-to
'       this has helpful information
'       auto filter on not equals

' create an array of arrays to apply filters

Function getUserDateInterval() As Date
    
    getUserDateInterval = CalendarForm.GetDate
    
    ' Confirm user input for date
    Dim ans As Integer
    ans = MsgBox("Are you sure with the selected date " + Str(dateVariable) & _
        vbNewLine & "Date is in format: month/day/year", vbYesNoCancel)
    
    ' Keep looping until user confirms input or user cancels macro
    Do While (ans <> 6)
        
        ' No (they are not sure of the selected date)
        ' Ask them to reselect a date
        If ans = 7 Then
            getUserDateInterval = CalendarForm.GetDate
            ans = MsgBox("Are you sure with the selected date " + Str(dateVariable) & _
                vbNewLine & "Date is in format: month/day/year", vbYesNoCancel)
                
        ' Cancel
        ElseIf ans = 2 Then
            getUserDateInterval = Null
            
        Else
            getUserDateInterval = Null
        End If
    Loop
End Function



Sub mainUpdateMonthlyStats()
    
    Dim totalEmployeeAUS As Range: Set totalEmployeeAUS = ActiveSheet.Range("C9")
    Dim totalEmployeeStates As Range: Set totalEmployeeStates = ActiveSheet.Range("C3")

    Dim totalSUBSAUS As Range: Set totalSUBSAUS = ActiveSheet.Range("D9")
    Dim totalSUBSStates As Range: Set totalSUBSStates = ActiveSheet.Range("D3")
    
    ' First set up the date
    ' Open user input for startingDate
    Dim startDate As Date
    Dim strStartDate As String
    Dim wbCurrent As Workbook
    Dim wsCurrent As Worksheet
    
    Dim elem As Variant
    Dim i As Integer: i = 0
    
    Dim arrStrStates As Variant: arrStrStates = Array("NSW", "VIC", "QLD", "WA", "SA", "ACT")
    
    
'    strStartDate = Application.WorksheetFunction.Text(getUserDateInterval(), "mm/dd/yyyy")
'    strEndDate = Application.WorksheetFunction.Text(getUserDateInterval(), "mm/dd/yyyy")
'
'    Range("B3").value = strStartDate
'    Range("B4").value = strEndDate
    
    Dim strFilePath As String: strFilePath = getFilePath()
    
    ' No file was chosen
    If (strFilePath = "") Then
        Exit Sub
    End If
    
    Set wbCurrent = Workbooks.Open(strFilePath)
    Set wsCurrent = wbCurrent.Worksheets(1)
    
    ' remove any currently used autofilters
    ' Initial base filters
    Call resetForNextSearch(wsCurrent)
    
    ' ------------ Getting Total Employee Counts --------
    totalEmployeeAUS.value = filterForEmployee(wsCurrent)
    
    i = 0
    For Each elem In arrStrStates
        totalEmployeeStates.Offset(i, 0).value = filterByState(wsCurrent, CStr(elem))
        i = i + 1
    Next elem
    
    Call resetForNextSearch(wsCurrent)
    ' ---------------------------------------------------
    
    ' ----------- Getting Total SUBS -------------------

    totalSUBSAUS.value = getVisibleFilterRowCount(wsCurrent) - totalEmployeeAUS.value
    
    i = 0
    For Each elem In arrStrStates
        totalSUBSStates.Offset(i, 0).value = filterByState(wsCurrent, CStr(elem)) - totalEmployeeStates.Offset(i, 0).value
        i = i + 1
    Next elem
    
    Call resetForNextSearch(wsCurrent)
    
    ' -----------------------------------------------
    
    Dim scoreCol As Integer: scoreCol = settings.Range("E31")
    
    ws.Range("E5") = Application.WorksheetFunction.Average(ws.Range("C:C"))
    
    
    ' Apply filter for particular task of focus
    '       Get Nation Totals
    
    '       for each: Get state totals
    
    
    
End Sub

Sub resetForNextSearch(ws As Worksheet)
    ' remove any currently used autofilters
    ' Initial base filters
    Call ClearFilter(ws)
    Call filterDate(ws)

End Sub


