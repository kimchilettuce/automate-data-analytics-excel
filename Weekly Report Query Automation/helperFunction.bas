Function openFile() As String
    On Error GoTo Macro1_Err

    Dim fd As Office.FileDialog
    Dim strFile As String: strFile = ""
     
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
     
    With fd
     
        .Filters.Clear
        .Filters.Add "Excel Files", "*.xlsx?", 1
        .Title = "Choose the Weekly Report"
        .AllowMultiSelect = False
     
        .InitialFileName = "C:\VBA Folder"
     
        If .Show = True Then
     
            strFile = .SelectedItems(1)
     
        End If
     
    End With

    openFile = strFile


Macro1_Exit:
    Exit Function

Macro1_Err:
    MsgBox Error$
    Resume Macro1_Exit

End Function
