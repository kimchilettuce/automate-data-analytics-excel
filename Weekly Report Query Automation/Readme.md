## Questions

qry_REPORtBooktoDWG

-   has an extra column popping up. It is different from the names listed in MDC Agent, maybe I can ask where it is coming from. Doesn't seem to be a duplicate
-   the excel ws has a 'raw lead' column that does not appear on the query. Where is it coming from
-   There are many hidden rows in the excel ws. Why is this so? It seems like the entries in the ws have filter applied to them, is this meant to be automatic when I transfer the data over from the query? Or, was this something that was manually done.

Worksheet Pivottables

-   how are these extra columns made. My idea is to simply update the data beneath the labels so as to now have to deal with how the pivot tables work. Just simply update the data and refresh. - but sometimes, things get lost when you press refresh, so I want to understand the process better
