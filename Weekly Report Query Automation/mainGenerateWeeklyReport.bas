Option Compare Database

Function exportQry(ByVal strQryName As String, oWb As Workbook) As Boolean
    
    On Error GoTo PROC_ERR_EXPORTQRY
    
    Dim oWs As Worksheet
    Dim oRs As Recordset
    
    ' make a new sheet name
    ' Open sheet with query name, then clear its contents
    Set oWs = oWb.Sheets(strQryName)
    Set oRs = CurrentDb.OpenRecordset(strQryName)
    
    ' by default, the query transfer passes
    exportQry = True
    
    Debug.Print "Starting to export query"
    
    ' Wipe all old data
    Rows("2:" & Rows.Count).ClearContents
    
    ' Fill in the field names in the first row
    ' fields.type:
    '       8 -> Date
    
    Debug.Print "Checking header labels now"
    
    For i = 0 To oRs.Fields.Count - 1
        
        ' If the label on worksheet, does not match label on query result
        ' then inform the user that the transfer of data won't match
        If (strFormat(oRs.Fields(i).Name) <> strFormat(oWs.Cells(1, i + 1).Value)) Then
            MsgBox oWs.Name + " has non-matching columns to the query. (Found on column " + Str(i + 1) + ")"
            exportQry = False
            GoTo Cleanup
        End If
        
        ' This is to correct the header to match the query
        ' We want to deactivate this, else the conflicting names doesn't allow pivot table to refresh
        ' oWs.Cells(1, i + 1) = oRs.Fields(i).Name
    Next i
        
    ' Fill in the contents starting from A2
    oWs.Cells(2, 1).CopyFromRecordset oRs
    oWs.Cells.EntireColumn.AutoFit
    oWs.Cells.EntireRow.AutoFit
    
    Debug.Print "Contents filled"

Cleanup:
    Set oRs = Nothing
    Set oWs = Nothing
    
    Exit Function
    
PROC_ERR_EXPORTQRY:

    Select Case Err.Number
    
    ' Ignores error caused by no entries found
    Case 1004
        Resume Next
    Case Else
        MsgBox "Error: (" & Err.Number & ") " & Err.Description, vbCritical
    End Select
    
End Function

' Later this macro will be invoked by a button on the frontend
Public Sub main()

    On Error GoTo PROC_ERR

    ' Setting up excel application
    Dim oExc As Excel.Application
    Dim oWb As Excel.Workbook
    
    Dim arrQrys As Variant
    Dim strQry As Variant
    
    ' First run the query, to make sure the results are updated?
    
    
    ' Open up target excel file
    Set oExc = CreateObject("Excel.Application")
    oExc.Visible = False
    Set oWb = oExc.Workbooks.Open("C:\Users\rahardki\Desktop\test export query\Weekly Report.xlsx")
    
    ' Queries to update unto the report "qry_REPORTBooktoDwg"
    arrQrys = Array("qry_REPORTBooktoDwg", "qry_REPORTWeightedHours")
    
    For Each strQry In arrQrys
        ' If exporting the query fails, then cleanup main and exit.
        If (exportQry(strQry, oWb) = False) Then
            GoTo Cleanup
        End If
    Next strQry
    
    ' refresh all pivot tables
    oWb.RefreshAll
    
    Debug.Print "Finished"
    
Cleanup:

    ' Save and close the file
    oWb.Close savechanges:=True
    
    Set oWb = Nothing
    oExc.Quit
    Set oExc = Nothing
    
    Exit Sub
    ' Task 1: export data to weekly report
    
    ' Call DoCmd.TransferSpreadsheet(acExport, acSpreadsheetTypeExcel12, "tbl_States", "C:\Users\rahardki\Desktop\test export query\Weekly Report.xlsx")
    
    ' Need to check if possible
    ' Can send to existing file, but needs;
    '       report to be closed
    '           need to find how to ensure file is closed
    '       if qryResult has same name as exisitng excelWS, it will overwrite the excelWS
    '           This might be desirable :) to get fresh updated results
    
    
    
    ' Task 2: fresh pivot tables
    ' Note the weekly report is in a fixed path
    '   Need to determine where things are in the folder
    ' Call runExcelMacro("C:\runMacro.xls", "Macro1")
    
    ' Task 3: clean up formating of weekly report
    '   Hide any dev setting sheets
    
    'S:\Aus_NI\Engineering\1. Admin\6. Shape Reporting\Weekly Reports\1. Combined Reports
    
    
    ' Notes
    '       if you have weekly report open, then it raises a prompt to save the file as a new copy
    '       if report is not open, changes and saved on the current.
    '       both should be ok.

PROC_ERR:

    Select Case Err.Number
    
    ' Ignores error caused by no entries found
    Case 1004
        Resume Next
    Case Else
        MsgBox "Error: (" & Err.Number & ") " & Err.Description, vbCritical
    End Select
End Sub



